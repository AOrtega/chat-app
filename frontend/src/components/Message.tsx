import React from 'react';

interface Props {
  user: string;
  message: string;
}

const Message = ({ user, message }: Props) => (
  <div
    style={{
      background: '#eee',
      borderRadius: '10px',
      padding: '15px',
      margin: '10px',
    }}
  >
    <p>
      <strong>{user} says: </strong>
      {message}
    </p>
  </div>
);

export default Message;
