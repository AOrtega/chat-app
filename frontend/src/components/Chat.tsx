import React, { useState, useEffect, useRef } from 'react';
import { HubConnectionBuilder } from '@microsoft/signalr';
import ChatWindow from './ChatWindow';
import ChatInput from './ChatInput';

const Chat = () => {
  const [chat, setChat] = useState<Array<string>>([]);
  const latestChat = useRef<Array<string>>([]);

  latestChat.current = chat;

  useEffect(() => {
    const connection = new HubConnectionBuilder()
      .withUrl('https://localhost:44325/hubs/chat')
      .withAutomaticReconnect()
      .build();

    connection
      .start()
      .then((result) => {
        console.log('connected');

        connection.on('ReceiveMessage', (message) => {
          const updatedChat = [...latestChat.current];
          updatedChat.push(message);

          setChat(updatedChat);
        });
      })
      .catch((e) => console.log('Connection Failed: ', e));
  }, []);

  const sendMessage = async (user: string, message: string) => {
    const ChatMessage = {
      user: user,
      message: message,
    };

    try {
      await fetch('https://localhost:44325/chat/messages', {
        method: 'POST',
        body: JSON.stringify(ChatMessage),
        headers: {
          'Content-Type': 'application/json',
        },
      });
    } catch (e) {
      console.log('Sending message failed', e);
    }
  };

  return (
    <div>
      <ChatWindow chat={chat} />
      <br />
      <ChatInput sendMessage={sendMessage} />
    </div>
  );
};

export default Chat;
