import React, { useState } from 'react';

interface Props {
  sendMessage: (user: string, message: string) => void;
}

const ChatInput = ({ sendMessage }: Props) => {
  const [user, setUser] = useState('');
  const [message, setMessage] = useState('');

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const isUserProvided = user && user !== '';
    const isMessageProvided = message && message !== '';

    if (isUserProvided && isMessageProvided) {
      sendMessage(user, message);
    } else {
      alert('Please insert a user and message.');
    }
  };

  const onUserUpdate = (e: React.FormEvent<HTMLInputElement>) => {
    setUser(e.currentTarget.value);
  };

  const onMessageUpdate = (e: React.FormEvent<HTMLInputElement>) => {
    setMessage(e.currentTarget.value);
  };

  return (
    <div style={{ position: 'fixed', bottom: '0', width: '100%' }}>
      <form onSubmit={onSubmit}>
        <div className="ui fluid action input">
          <input
            id="user"
            name="user"
            placeholder="What is your name?"
            value={user}
            onChange={onUserUpdate}
          />
        </div>
        <div className="ui fluid action input">
          <input
            type="text"
            id="message"
            name="message"
            placeholder="Message"
            value={message}
            onChange={onMessageUpdate}
          />
          <button className="ui animated primary button">
            <div className="hidden content">Send</div>
            <div className="visible content">
              <i className="paper plane icon"></i>
            </div>
          </button>
        </div>
      </form>
    </div>
  );
};

export default ChatInput;
