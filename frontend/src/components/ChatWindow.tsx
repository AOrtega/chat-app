import React from 'react';
import Message from './Message';

interface Props {
  chat: Array<string>;
}

const ChatWindow = ({ chat }: Props) => {
  const conversation = chat.map((m: any) => (
    <Message
      key={Date.now() * Math.random()}
      user={m.user}
      message={m.message}
    />
  ));

  return <div>{conversation}</div>;
};

export default ChatWindow;
